//სტუდენტების აღწერა #1
         let students = [];
        students[0] = {
        firstname: 'Jan',
        lastname: 'Reno',
        age: 26,
        scores: {
            javascript: 62,
            react: 57,
             python: 88,
            java: 90
        }
        };


    students[1] = {
        firstname: 'Claude',
        lastname: 'Monet',
        age: 19,
        scores: {
            javascript: 77,
            react: 52,
            python: 92,
            java: 67
        }
        
        };
    
    students[2] = {
        firstname: 'Van',
        lastname: 'Gogh',
        age: 21,
        scores: {
            javascript: 51,
            react: 98,
            python: 65,
            java: 70
        }
        };

        students[3] = {
        firstname: 'Dam',
        lastname: 'Square',
        age: 36,
        scores: {
        javascript: 82,
            react: 53,
            python: 80,
                java: 65
        }
    };

// #2 დავალება : ქულების ჯამი, ქულების საშუალო და GPA
//ქულების ჯამი 
  students[0].sum = students[0].scores.javascript + students[0].scores.react
  + students[0].scores.python + students[0].scores.java;

  students[1].sum = students[1].scores.javascript + students[1].scores.react
  + students[1].scores.python + students[1].scores.java;

  students[2].sum= students[2].scores.javascript + students[2].scores.react
  + students[2].scores.python + students[2].scores.java;
  
  students[3].sum = students[3].scores.javascript + students[3].scores.react
  + students[3].scores.python + students[3].scores.java;

  console.log("Sum Of The Points : ");

  let sumOfPoints = `First Student's Sum Of Points Is - ${students[0].sum},
  For The Second Student It Is - ${students[1].sum}, 
  The Third Student Has - ${students[2].sum}
  And For The Fourth Student The Sum Is - ${students[3].sum}`;

  console.log(sumOfPoints);

  //ქულების საშუალო 
  students[0].average = (students[0].scores.javascript + students[0].scores.react
    + students[0].scores.python + students[0].scores.java)/4;

    students[1].average = (students[1].scores.javascript + students[1].scores.react
        + students[1].scores.python + students[1].scores.java)/4;  

  students[2].average = ( students[2].scores.javascript + students[2].scores.react
    + students[2].scores.python + students[2].scores.java)/4;

  students[3].average = (students[3].scores.javascript + students[3].scores.react
    + students[3].scores.python + students[3].scores.java)/4;

  console.log("Average Of The Points : ");

  let averagePoints = `First Student's Average Of Points - ${students[0].average},
  For The Second Students - ${students[1].average}, 
  The Third Student Has - ${students[2].average}
  And For The Fourth Student Average Is - ${students[3].average}`;

  console.log(averagePoints);

  //GPA გამოთვლა 
let credits = 4 + 7 + 6 + 3;
students[0].gpa= 
   (students[0].scores.javascript * 4 + students[0].scores.react * 7
+ students[0].scores.python * 6 + students[0].scores.java * 3)/credits
; 

students[1].gpa= 
   (students[1].scores.javascript * 4 + students[1].scores.react * 7
+ students[1].scores.python * 6 + students[1].scores.java * 3)/credits
; 

students[2].gpa= 
(students[2].scores.javascript * 4 + students[2].scores.react * 7
+ students[2].scores.python * 6 + students[2].scores.java * 3)/credits
; 


students[3].gpa= 
(students[3].scores.javascript * 4 + students[3].scores.react * 7
+ students[3].scores.python * 6 + students[3].scores.java * 3)/credits
; 

 console.log("GPA : ");

 let studentsGpa = `First Student's GPA  - ${students[0].gpa},
Second Student's GPA- ${students[1].gpa}, 
 Third Student's GPA - ${students[2].gpa}
  Fourth Student's GPA - ${students[3].gpa}`;

  console.log(studentsGpa);
//პირველი სტუდენტის GPA
let GPA1 = 0;
if(students[0].gpa >=51 && students[0].gpa  <=60){
    GPA1 = 0.5;
    console.log("GPA : " + GPA1);
}
else if(students[0].gpa  >=61 && students[0].gpa  <=70){
    GPA1 = 1;
    console.log("GPA : " + GPA1);
}
else if(students[0].gpa  >=71 && students[0].gpa  <=80){
    GPA1 = 2;
    console.log("GPA : " + GPA1);
}
else if(students[0].gpa  >=81 && students[0].gpa  <=90){
    GPA1 = 3;
    console.log("GPA : " + GPA1);
}
else if(students[0].gpa  >=91 && students[0].gpa  <=100){
    GPA1 = 4;
    console.log("GPA : " + GPA1);
}

//მეორე სტუდენტის GPA
let GPA2 = 0;
if(students[1].gpa  >=51 && students[1].gpa <=60){
    GPA2 = 0.5;
    console.log("GPA : " + GPA2);
}
else if(students[1].gpa >=61 && students[1].gpa <=70){
    GPA2 = 1;
    console.log("GPA : " + GPA2);
}
else if(students[1].gpa >=71 && students[1].gpa <=80){
    GPA2 = 2;
    console.log("GPA : " + GPA2);
}
else if(students[1].gpa >=81 && students[1].gpa <=90){
    GPA2 = 3;
    console.log("GPA : " + GPA2);
}
else if(students[1].gpa >=91 && students[1].gpa <=100){
    GPA2 = 4;
    console.log("GPA : " + GPA2);
}

//მესამე სტუდენტის GPA
let GPA3 = 0;
if(students[2].gpa >=51 && students[2].gpa <=60){
    GPA3 = 0.5;
    console.log("GPA : " + GPA3);
}
else if(students[2].gpa >=61 && students[2].gpa <=70){
    GPA3 = 1;
    console.log("GPA : " + GPA3);
}
else if(students[2].gpa >=71 && students[2].gpa <=80){
    GPA3 = 2;
    console.log("GPA : " + GPA3);
}
else if(students[2].gpa >=81 && students[2].gpa <=90){
    GPA3 = 3;
    console.log("GPA : " + GPA3);
}
else if(students[2].gpa >=91 && students[2].gpa <=100){
    GPA3 = 4;
    console.log("GPA : " + GPA3);
}


//მეოთხე სტუდენტის GPA
let GPA4 = 0;
if(students[3].gpa >=51 && students[3].gpa <=60){
    GPA4 = 0.5;
    console.log("GPA : " + GPA4);
}
else if(students[3].gpa >=61 && students[3].gpa <=70){
    GPA4 = 1;
    console.log("GPA : " + GPA4);
}
else if(students[3].gpa >=71 && students[3].gpa <=80){
    GPA4 = 2;
    console.log("GPA : " + GPA4);
}
else if(students[3].gpa >=81 && students[3].gpa <=90){
    GPA4 = 3;
    console.log("GPA : " + GPA4);
}
else if(students[3].gpa >=91 && students[3].gpa <=100){
    GPA4 = 4;
    console.log("GPA : " + GPA4);
}



  // დავალება #3 : საერთო საშ. არითმ. და სტუდენტთა სტატუსები

  let AverageOfstudents = (students[0].average + students[1].average
   + students[2].average + students[3].average)/4;
  
  console.log(AverageOfstudents);
//პირველი სტუდენტის სტატუსი 
  if(students[0].average > AverageOfstudents){
      console.log("წითელი დიპლომის მქონე");
  }else{
    console.log("ვრაგ ნაროდა");
  }
//მეორე სტუდენტის სტატუსი
if(students[1].average > AverageOfstudents){
    console.log(" წითელი დიპლომის მქონე");
}else{
  console.log(" ვრაგ ნაროდა");
}

//მესამე სტუდენტის სტატუსი
if(students[2].average  > AverageOfstudents){
    console.log(" წითელი დიპლომის მქონე");
}else{
  console.log(" ვრაგ ნაროდა");
}

//მეოთხე სტუდენტის სტატუსი
if(students[3].average > AverageOfstudents){
    console.log(" წითელი დიპლომის მქონე");
}else{
  console.log(" ვრაგ ნაროდა");
}

// დავალება #4 : საუკეთესო GPA-ის მქონე სტუდენტთა დადგენა
let maxGPA;
maxGPA = GPA1;
if(maxGPA < GPA2){
    maxGPA = GPA2;
    console.log("Second Student Has The Highets GPA: " + maxGPA);
}
else if(maxGPA < GPA3){
    maxGPA = GPA3;
    console.log("Third Student Has The Highets GPA: " + maxGPA);
}
else if(maxGPA < GPA4){
    maxGPA = GPA4;
    console.log("Fourth Student Has The Highets GPA: " + maxGPA);
}
else if(maxGPA === GPA2){
if(maxGPA === GPA3){   
console.log("First,Second And Third Students Have Same GPA : " + maxGPA);
}
}
else if(maxGPA === GPA3){
    if(maxGPA === GPA4){   
    console.log("First,Third And Fourth Students Have Same GPA : " + maxGPA);
    }
    }
else if(maxGPA === GPA2){
    if(maxGPA === GPA4){   
console.log("First,Second And Fourth Students Have Same GPA : " + maxGPA);
}
}  

else if(maxGPA === GPA2){
    console.log("First And Second Students Have Same GPA : " + maxGPA);
}
else if(maxGPA === GPA3){
    console.log("First And Third Students Have Same GPA : " + maxGPA);
}
else if(maxGPA === GPA4){
    console.log("First And Fourth Students Have Same GPA : " + maxGPA);
}

else{
    console.log("First Student Has The Highets GPA: " + maxGPA);
}

//დავალება #5 : საუკეთესო საშუალო ქულის მქონე 21 + სტუდენტი
if(students[0].age >= 21 && students[0].average > students[1].average && students[0].average > students[2].average 
    && students[0].average > students[3].average ){
        console.log("First Student Has The Best Average Of Point");
    }

    if(students[1].age >= 21 && students[1].average > students[1].average && students[1].average > students[2].average 
        && students[1].average > students[3].average ){
            console.log("First Student Has The Best Average Of Point");
        }
        if(students[2].age >= 21 && students[2].average > students[1].average && students[2].average > students[2].average 
            && students[2].average > students[3].average ){
                console.log("First Student Has The Best Average Of Point");
            }

            if(students[3].age >= 21 && students[3].average > students[1].average && students[3].average > students[2].average 
                && students[3].average > students[3].average ){
                    console.log("First Student Has The Best Average Of Point");
                }
// დავალება #6 : საუკეთესო სტუდენტი ფრონტ-ენდის საგნებში 
//საშუალო ქულების მიხედვით (js, react)

//ფრონტ-ენდში საშუალოების დათვლა თითოეული სტუდენტისათვის
let max = "First Student";
students[0].frontEnd = (students[0].scores.javascript + students[0].scores.react)/2;
students[1].frontEnd = (students[1].scores.javascript + students[1].scores.react)/2;
students[2].frontEnd = (students[2].scores.javascript + students[2].scores.react)/2;
students[3].frontEnd = (students[3].scores.javascript + students[3].scores.react)/2;
console.log("Average Points In Front-End Subjects Are: ");
console.log(students[0].frontEnd,students[1].frontEnd,students[2].frontEnd,students[3].frontEnd);
//ფრონტ-ენდში მაქსიმალური საშუალო ქულის მქონე სტუდენტის დადგენა
let max_average_front_end = students[0].frontEnd;

if(max_average_front_end < students[1].frontEnd){
    max_average_front_end = students[1].frontEnd;
    max = "Second Student";
   
}
 if(max_average_front_end < students[2].frontEnd){
    max_average_front_end = students[2].frontEnd;
    max = "Third Student";
   
}
if(max_average_front_end < students[3].frontEnd){
    max_average_front_end = students[3].frontEnd;
    max = "Fourth Student";
    
}

console.log("Highest Average In Front-End has " + max + " and his/her point is " + max_average_front_end);